# Final exam practical implementation 
# 
# Course: Digital image processing
# Created by: Michael Konstantinou


from PyQt5 import QtCore, QtGui, QtWidgets
import sys
import cv2
import os
import numpy as np
import math
from matplotlib import pyplot as plt

# Import core modifications helpers
import jpeg_helpers.core_mods as jpeg_core
import jpeg_helpers.jpeg as jpeg
import convolution_helpers.core_mods as convolution_core
import video_helpers.core_mods as video_core
import fourier_helpers.core_mods as fourier_core

# Custom utilities
import utils.file as file
import utils.dialogs as dialogs
import utils.modificators as core

# Configuration files
import config.settings as settings


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(570, 333)
        Form.setStyleSheet("* {\n"
"    font: 14pt \"Segoe UI\";\n"
"}\n"
"\n"
"QPushButton {\n"
"    background-color: #007bff;\n"
"    color: white;\n"
"    border: none;\n"
"    padding: 10px;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    background-color: #0051a8;\n"
"}\n"
"\n"
"#lbl_copyrights {\n"
"    font-size: 10px;\n"
"}")
        self.gridLayout = QtWidgets.QGridLayout(Form)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(Form)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.txt_path = QtWidgets.QLineEdit(Form)
        self.txt_path.setObjectName("txt_path")
        self.horizontalLayout.addWidget(self.txt_path)
        self.btn_browse = QtWidgets.QPushButton(Form)
        self.btn_browse.setObjectName("btn_browse")
        self.horizontalLayout.addWidget(self.btn_browse)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_2 = QtWidgets.QLabel(Form)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_2.addWidget(self.label_2)
        self.lineEdit = QtWidgets.QLineEdit(Form)
        self.lineEdit.setObjectName("lineEdit")
        self.horizontalLayout_2.addWidget(self.lineEdit)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_3 = QtWidgets.QLabel(Form)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_3.addWidget(self.label_3)
        self.lineEdit_2 = QtWidgets.QLineEdit(Form)
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.horizontalLayout_3.addWidget(self.lineEdit_2)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.lbl_copyrights = QtWidgets.QLabel(Form)
        self.lbl_copyrights.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_copyrights.setObjectName("lbl_copyrights")
        self.verticalLayout.addWidget(self.lbl_copyrights)
        self.btn_submit = QtWidgets.QPushButton(Form)
        self.btn_submit.setObjectName("btn_submit")
        self.verticalLayout.addWidget(self.btn_submit)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Michael Konstantinou - 1005611"))
        self.label.setText(_translate("Form", "Video path:"))
        self.btn_browse.setText(_translate("Form", "Browse"))
        self.label_2.setText(_translate("Form", "Choose frame 1: "))
        self.label_3.setText(_translate("Form", "Choose frame 2: "))
        self.lbl_copyrights.setText(_translate("Form", "Made by Michael Konstantinou in December 2020 for the purposes of the course \"Digital image processing\""))
        self.btn_submit.setText(_translate("Form", "Submit"))

        self.customConfiguration()

    def customConfiguration(self):
        """
        The method is responsible for assigning events, signals, slots and threads if exist. 
        It also declares the initialization global variables
        """

        # Filename for image
        self.selectedFile = file.File()

        # Signals & slots
        self.btn_browse.clicked.connect(self.browseSlot)
        self.btn_submit.clicked.connect(self.submitSlot)

    def browseSlot(self):
        """
        Displays browse dialog. When file is selected continues with the
        image processing tasks.
        """

        filename, _ = QtWidgets.QFileDialog.getOpenFileName(None, "Browse", "", "All Files (*)")
        if filename:
            self.selectedFile.set_file_name(filename)
            self.txt_path.setText(self.selectedFile.get_file_name())

    def set_plot_image(self, position, title, img, color_map='gray', bar=False):
        """
        Sets an image to a specified position, of a matplotlib window.
        """

        plt.subplot(position)
        plt.title(title)

        # Remove ticks
        plt.xticks([])
        plt.yticks([])

        plt.imshow(img, cmap=color_map)
        if bar:
            plt.colorbar(fraction=0.03, pad=0.04)

    def set_plot_image_twelve(self, position, title, img, color_map='gray', bar=False):
        """
        Sets an image to a specified position, of a matplotlib window.
        """

        plt.subplot(4, 3, position)
        plt.title(title)

        # Remove ticks
        plt.xticks([])
        plt.yticks([])

        plt.imshow(img, cmap=color_map)
        if bar:
            plt.colorbar(fraction=0.03, pad=0.04)

    def create_absolute_diff_image(self, img1, img2, N, M):
        diff = np.zeros((N, M))
        for i in range(0, N):
            for j in range(0, M):
                diff[i, j] = abs(i - j)
        return diff

    def submitSlot(self):
        """
        Handles all operations asked when pressing the submit button
        """

        video_path = self.selectedFile.get_file_name()

        # Check if path is empty
        if video_path is None:
            dialogs.show_msg('Please select video')
            return

        if self.lineEdit.text() is None or self.lineEdit.text() == '':
            print("Please give frame 1. No frame is provided")
            dialogs.show_msg('Please give frame 1. No frame is provided')
            return

        if self.lineEdit_2.text() is None or self.lineEdit_2.text() == '':
            print("Please give frame 2. No frame is provided")
            dialogs.show_msg('Please give frame 2. No frame is provided')
            return

        # Read frame number
        frame_one = int(self.lineEdit.text())
        frame_two = int(self.lineEdit_2.text())

        # Read frames from video
        img_frame_one = video_core.get_frame(video_path, frame_one)
        if img_frame_one is None:
            dialogs.show_msg('Not valid video or frame one out of bounds')
            return

        img_frame_two = video_core.get_frame(video_path, frame_two)
        if img_frame_two is None:
            dialogs.show_msg('Not valid video or frame two out of bounds')
            return

        # Apply canny to both frames
        img_frame_one = core.to_grayscale(img_frame_one)
        img_frame_two = core.to_grayscale(img_frame_two)
        img_one_with_canny = core.canny(img_frame_one)
        img_two_with_canny = core.canny(img_frame_two)

        # Apply laplacian and sobel
        img_one_with_laplacian = convolution_core.filter_img(img_frame_one, 'Laplacian')
        img_two_with_laplacian = convolution_core.filter_img(img_frame_two, 'Laplacian')
        img_one_with_sobel = convolution_core.filter_img(img_frame_one, 'Sobel')
        img_two_with_sobel = convolution_core.filter_img(img_frame_two, 'Sobel')

        # Calculate absolute difference
        img_n = img_frame_one.shape[0]
        img_m = img_frame_one.shape[1]
        img_abs = abs(img_frame_one - img_frame_two)
        img_abs_canny = abs(img_one_with_canny - img_two_with_canny)
        img_abs_laplacian = abs(img_one_with_laplacian - img_two_with_laplacian)
        img_abs_sobel = abs(img_one_with_sobel - img_two_with_sobel)

        # Show results on screen
        plt.figure(settings.WINDOW_TITLE)
        self.set_plot_image_twelve(1, 'Video frame ' + str(frame_one),img_frame_one)
        self.set_plot_image_twelve(2, 'Video frame ' + str(frame_two), img_frame_one)
        self.set_plot_image_twelve(3, 'Absolute difference', img_abs)
        self.set_plot_image_twelve(4, 'Canny - frame ' + str(frame_one), img_one_with_canny)
        self.set_plot_image_twelve(5, 'Canny - frame ' + str(frame_two), img_two_with_canny)
        self.set_plot_image_twelve(6, 'Absolute difference (canny)', img_abs_canny)
        self.set_plot_image_twelve(7, 'Laplacian - frame ' + str(frame_one), img_one_with_laplacian)
        self.set_plot_image_twelve(8, 'Laplacian - frame ' + str(frame_two), img_two_with_laplacian)
        self.set_plot_image_twelve(9, 'Absolute difference (Laplacian)', img_abs_laplacian)
        self.set_plot_image_twelve(10, 'Sobel - frame ' + str(frame_one), img_one_with_sobel)
        self.set_plot_image_twelve(11, 'Sobel - frame ' + str(frame_two), img_two_with_sobel)
        self.set_plot_image_twelve(12, 'Absolute difference (Sobel)', img_abs_sobel)
        plt.show()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())
