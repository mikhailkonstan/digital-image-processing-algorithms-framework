"""
This class is responsible for all core operations that involve video manipulation

Created by: Michael Konstantinou
"""

import cv2
from matplotlib import pyplot as plt
import numpy as np
from skimage.metrics import structural_similarity as ssim
import os

def get_frame(video_path, frame_num):
    """
    Read a video and return a specific frame.
    NOTE: The first frame of a video is 1.
    """

    if frame_num <= 0:
        print('Error: The first frame is 1')
        return None

    video_cap = cv2.VideoCapture(video_path)

    video_length = int(video_cap.get(cv2.CAP_PROP_FRAME_COUNT))
    if frame_num > video_length:
        print('Error: The maximum frame is ' + str(video_length))
        return None

    success, img_frame = video_cap.read()
    count = 1
    while success:
        if count == frame_num:
            return img_frame
        success, img_frame = video_cap.read()
        count += 1

def get_frame_fast(video_path, frame_number):
    """
    Returns specified frame without checking for mistakes
    """
    
    cap = cv2.VideoCapture(video_path)
    cap.set(cv2.CAP_PROP_POS_FRAMES, frame_number - 1)  # get spesific frame
    res, frame = cap.read()
    return frame