# Final exam practical implementation 
# 
# Course: Digital image processing
# Created by: Michael Konstantinou


from PyQt5 import QtCore, QtGui, QtWidgets
import sys
import cv2
import os
import numpy as np
import math
from matplotlib import pyplot as plt

# Import core modifications helpers
import jpeg_helpers.core_mods as jpeg_core
import jpeg_helpers.jpeg as jpeg
import convolution_helpers.core_mods as convolution_core
import video_helpers.core_mods as video_core
import fourier_helpers.core_mods as fourier_core

# Custom utilities
import utils.file as file
import utils.dialogs as dialogs
import utils.modificators as core

# Configuration files
import config.settings as settings


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(570, 630)
        Form.setStyleSheet("* {\n"
"    font: 14pt \"Segoe UI\";\n"
"}\n"
"\n"
"QPushButton {\n"
"    background-color: #007bff;\n"
"    color: white;\n"
"    border: none;\n"
"    padding: 10px;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    background-color: #0051a8;\n"
"}\n"
"\n"
"#lbl_copyrights {\n"
"    font-size: 10px;\n"
"}")
        self.gridLayout = QtWidgets.QGridLayout(Form)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(Form)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.txt_path = QtWidgets.QLineEdit(Form)
        self.txt_path.setObjectName("txt_path")
        self.horizontalLayout.addWidget(self.txt_path)
        self.btn_browse = QtWidgets.QPushButton(Form)
        self.btn_browse.setObjectName("btn_browse")
        self.horizontalLayout.addWidget(self.btn_browse)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_2 = QtWidgets.QLabel(Form)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_2.addWidget(self.label_2)
        self.txtframe = QtWidgets.QLineEdit(Form)
        self.txtframe.setObjectName("txtframe")
        self.horizontalLayout_2.addWidget(self.txtframe)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.lbl_copyrights = QtWidgets.QLabel(Form)
        self.lbl_copyrights.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_copyrights.setObjectName("lbl_copyrights")
        self.verticalLayout.addWidget(self.lbl_copyrights)
        self.btn_submit = QtWidgets.QPushButton(Form)
        self.btn_submit.setObjectName("btn_submit")
        self.verticalLayout.addWidget(self.btn_submit)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Michael Konstantinou - 1005611"))
        self.label.setText(_translate("Form", "Video path:"))
        self.btn_browse.setText(_translate("Form", "Browse"))
        self.label_2.setText(_translate("Form", "Choose frame"))
        self.lbl_copyrights.setText(_translate("Form", "Made by Michael Konstantinou in December 2020 for the purposes of the course \"Digital image processing\""))
        self.btn_submit.setText(_translate("Form", "Submit"))

        self.customConfiguration()

    def browseSlot(self):
            """
            Displays browse dialog. When file is selected continues with the
            image processing tasks.
            """

            filename, _ = QtWidgets.QFileDialog.getOpenFileName(None, "Browse", "", "All Files (*)")
            if filename:
                self.selectedFile.set_file_name(filename)
                self.txt_path.setText(self.selectedFile.get_file_name())

    def print_stats(self, init_img, img_comp, filter, noise):
        
        # Calculate statistics for each photo
        mse = jpeg_core.mse_func(init_img, img_comp)
        psnr = jpeg_core.psnr_func(mse)
        ssim = jpeg_core.ssim_func(init_img, img_comp)

        print('Filter: ' + filter)
        print('Noise ' + noise)
        print('MSE: ' + str(mse))
        print('PSNR: ' + str(psnr))
        print('SSIM: ' + str(ssim))
        print('---------------------')

    def submitSlot(self):

        video_path = self.selectedFile.get_file_name()

        # Check if path is empty
        if video_path is None:
            dialogs.show_msg('Please select video')
            return

        if self.txtframe.text() is None or self.txtframe.text() == '':
            print("Error please give a frame")
            dialogs.show_msg('Error please give a frame')
            return

        # Read frame number
        frame_num = int(self.txtframe.text())

        img_frame = video_core.get_frame(video_path, frame_num)
        if img_frame is None:
            dialogs.show_msg('Not valid video or frame out of bounds')
            return

        img_frame = core.to_grayscale(img_frame)
        img_gaussian = convolution_core.add_noise(img_frame, 'Gaussian')
        img_salt_and_pepper = convolution_core.add_noise(img_frame, 'Salt & pepper')
        img_speckle = convolution_core.add_noise(img_frame, 'Speckle')

        # Apply averaging filter for each image
        avg_gaussian = convolution_core.filter_img(img_gaussian, 'Averaging')
        avg_salt_and_pepper = convolution_core.filter_img(img_salt_and_pepper, 'Averaging')
        avg_speckle = convolution_core.filter_img(img_speckle, 'Averaging')

        # Apply gaussian filter for each image
        gau_gaussian = convolution_core.filter_img(img_gaussian, 'Gaussian')
        gau_salt_and_pepper = convolution_core.filter_img(img_salt_and_pepper, 'Gaussian')
        gau_speckle = convolution_core.filter_img(img_speckle, 'Gaussian')

        # Apply gaussian filter for each image
        med_gaussian = convolution_core.filter_img(img_gaussian, 'Median')
        med_salt_and_pepper = convolution_core.filter_img(img_salt_and_pepper, 'Median')
        med_speckle = convolution_core.filter_img(img_speckle, 'Median')

        # Print statistics
        self.print_stats(img_gaussian, avg_gaussian, 'Averaging', 'Gaussian')
        self.print_stats(img_salt_and_pepper, avg_salt_and_pepper, 'Averaging', 'Salt & pepper')
        self.print_stats(img_speckle, avg_speckle, 'Averaging', 'Speckle')

        self.print_stats(img_gaussian, gau_gaussian, 'Gaussian', 'Gaussian')
        self.print_stats(img_salt_and_pepper, gau_salt_and_pepper, 'Gaussian', 'Salt & pepper')
        self.print_stats(img_speckle, gau_speckle, 'Gaussian', 'Speckle')

        self.print_stats(img_gaussian, avg_gaussian, 'Median', 'Gaussian')
        self.print_stats(img_salt_and_pepper, avg_salt_and_pepper, 'Median', 'Salt & pepper')
        self.print_stats(img_speckle, avg_speckle, 'Median', 'Speckle')

        plt.figure(settings.WINDOW_TITLE)
        self.set_plot_image_twelve(1, 'Gaussian noise', img_gaussian)
        self.set_plot_image_twelve(2, 'Salt & pepper noise', img_salt_and_pepper)
        self.set_plot_image_twelve(3, 'Speckle noise', img_speckle)
        self.set_plot_image_twelve(4, 'Gaussian noise + Averaging', avg_gaussian)
        self.set_plot_image_twelve(5, 'Salt & pepper noise + Averaging', avg_salt_and_pepper)
        self.set_plot_image_twelve(6, 'Speckle noise + Averaging', avg_speckle)
        self.set_plot_image_twelve(7, 'Gaussian noise + Gaussian', gau_gaussian)
        self.set_plot_image_twelve(8, 'Salt & pepper noise + Gaussian', gau_salt_and_pepper)
        self.set_plot_image_twelve(9, 'Speckle noise + Gaussian', gau_speckle)
        self.set_plot_image_twelve(10, 'Gaussian noise + Median', med_gaussian)
        self.set_plot_image_twelve(11, 'Salt & pepper noise + Median', med_salt_and_pepper)
        self.set_plot_image_twelve(12, 'Speckle noise + Median', med_speckle)
        plt.show()

    def set_plot_image_twelve(self, position, title, img, color_map='gray', bar=False):
            """
            Sets an image to a specified position, of a matplotlib window.
            """

            plt.subplot(4, 3, position)
            plt.title(title)

            # Remove ticks
            plt.xticks([])
            plt.yticks([])

            plt.imshow(img, cmap=color_map)
            if bar:
                plt.colorbar(fraction=0.03, pad=0.04)

    def set_plot_image(self, position, title, img, color_map='gray', bar=False):
            """
            Sets an image to a specified position, of a matplotlib window.
            """

            plt.subplot(position)
            plt.title(title)

            # Remove ticks
            plt.xticks([])
            plt.yticks([])

            plt.imshow(img, cmap=color_map)
            if bar:
                plt.colorbar(fraction=0.03, pad=0.04)

    def customConfiguration(self):
            """
            The method is responsible for assigning events, signals, slots and threads if exist. 
            It also declares the initialization global variables
            """

            # Filename for image
            self.selectedFile = file.File()

            # Signals & slots
            self.btn_browse.clicked.connect(self.browseSlot)
            self.btn_submit.clicked.connect(self.submitSlot)


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())
