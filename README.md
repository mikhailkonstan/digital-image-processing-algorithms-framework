# Digital Image Processing - Algorithms Framework

## Summary
This repository is a collection of all algorithms used during the course "Digital Image Processing". This was created mainly to help me out for my **practical examinations**. Thus, in this repository you will find the solution I gave at the final exam

### :computer: Technologies used:
- PyQt5
- OpenCV
- Python

## Other information
- :trophy: Highest graded exam
- :dart: Set as an example for future **Digital Image Processing** students at the University of Cyprus

## :open_file_folder: Structure
- Utils
Contains fundamental and global functions related to:
  - Basic global universal functions (such as convert to grayscale, binary, save image etc...)
  - File reader
  - Dialog message for pyqt5
- Helpers
Each file with the ending **_helpers** means that the folder contains all algorithms developed in an assignment that had to implement the specified functionality. 
eg. fourier_helpers contains all algorithms used during the Fourier assignment evaluation such as *fourier transform*

- Thema1.py
- Thema2.py
contain the solution of the first and second exam topics

- Config
Contains basic configuration settings such as the appearing window title

### Other files
- cat.mp4 
The video file that was used for the processing
- report.docx
Contains image results and report status

