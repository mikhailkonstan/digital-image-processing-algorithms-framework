"""
This class is responsible for all core operations that involve image manipulation

Created by: Michael Konstantinou, Constandinos Demetriou, George Hadjiantonis
"""
import cv2
from matplotlib import pyplot as plt
import numpy as np
from skimage.metrics import structural_similarity as ssim
import os

# Imports PIL module
from PIL import Image

import config.settings as settings

def get_shape(img):
    """
    Returns the shape of an image as tuplet
    """

    return img.shape


def get_size(img):
    """
    Returns the size of an image as integer
    """

    return img.size

def to_grayscale(img):
    """
    Convert image to grayscale.
    """

    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return img_gray

def read_image_color(path):
    """ 
    I always forget the values of this function so I made a new one :o 
    """

    return cv2.imread(path)

def read_image_grayscale(path):
    """ 
    I always forget the values of this function so I made a new one :o 
    """

    return cv2.imread(path, 0)

def show_image(img):
    """ 
    Shows a single image and waits for next key
    """

    # Show image using OpenCV
    cv2.imshow(settings.WINDOW_TITLE, img)

    # Hold image window open
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def save_img(img, img_name):
    """
    Saves a image.
    """
    tmp = cv2.normalize(img, dst=None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    tmp_img = Image.fromarray(tmp)
    tmp_img.save(img_name)

def save_img_as_tif(img_path):
    
    if img_path[-4:] != '.tif':
        img_orig = cv2.imread(img_path)
        img_path = img_path[:-4] + '.tif'
        cv2.imwrite(img_path, img_orig)

def convert_image_for_plotting(img):
    return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    
def canny(img_gray, minVal=100, maxVal=200):
    """
    Apply Canny algorithm to find the edges of an image.
    """

    return cv2.Canny(img_gray, minVal, maxVal)


