from PyQt5.QtWidgets import QMessageBox
import config.settings as settings

def show_msg(msg):
    """
    Appears a message in a window in case that an error occurred.
    """

    msg_box = QMessageBox()
    msg_box.setWindowTitle(settings.WINDOW_TITLE)
    msg_box.setText(msg)
    x = msg_box.exec_()