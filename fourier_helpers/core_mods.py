"""
This class is responsible for all core operations that involve image manipulation

Created by: Michael Konstantinou
"""
import cv2
import numpy as np
import math

def fft(img):
    """
    Apply fourier transform, mask and inverse fourier transform.
    """
    # Discrete Fourier transform
    dft = cv2.dft(np.float32(img), flags=cv2.DFT_COMPLEX_OUTPUT)
    # Shift the zero-frequency component (DC component) to the center of the spectrum
    dft_shift = np.fft.fftshift(dft)
    # Apply log transformation to the magnitude spectrum
    magnitude_spectrum = np.log(1 + cv2.magnitude(dft_shift[:, :, 0], dft_shift[:, :, 1]))

    return magnitude_spectrum

def separate_fg_bg(img_gray):
    """
    Separates background and foreground of the given image.
    """

    # Convert image to black and white
    img_bin = to_binary(img_gray)
    # Apply bitwise not to find the complement of image
    img_bin_inv = cv2.bitwise_not(img_bin)

    # Get background
    img_bg = cv2.bitwise_and(img_gray, img_bin, mask=None)

    # Get foreground
    img_fg = cv2.bitwise_and(img_gray, img_bin_inv, mask=None)
    img_fg[img_fg == 0] = 255

    return img_bg, img_fg

def crop_image(img, region_m, region_n):
    """
    Crop image in specific regions.
    """
    rows, cols = img.shape
    crow, ccol = (rows / 2, cols / 2)
    return img[int(crow - region_m / 2):int(crow + region_m / 2), int(ccol - region_n / 2):int(ccol + region_n / 2)]


def fourier_fun(img, mask_type, radius_a, radius_b=None):
    """
    Apply fourier transform, mask and inverse fourier transform.
    """
    # Discrete Fourier transform
    dft = cv2.dft(np.float32(img), flags=cv2.DFT_COMPLEX_OUTPUT)
    # Shift the zero-frequency component (DC component) to the center of the spectrum
    dft_shift = np.fft.fftshift(dft)
    # Apply log transformation to the magnitude spectrum
    magnitude_spectrum = np.log(1 + cv2.magnitude(dft_shift[:, :, 0], dft_shift[:, :, 1]))

    rows, cols = img.shape
    crow, ccol = (rows / 2, cols / 2)
    y, x = np.ogrid[-crow:crow, -ccol:ccol]

    mask = np.zeros((rows, cols, 2), np.uint8)
    mask[np.sqrt(x ** 2 + y ** 2) <= radius_a] = 1

    if mask_type == 'HighPass':
        # inverse zeroes and ones
        mask = 1 - mask
    elif mask_type == 'MidPass':
        # invrerse mask and set values outside the second radius to zero
        mask = 1 - mask
        mask[np.sqrt(x ** 2 + y ** 2) > radius_b] = 0

    # apply mask
    fshift = dft_shift * mask
    # inverse DFT
    f_ishift = np.fft.ifftshift(fshift)
    img_back = cv2.idft(f_ishift)
    img_back = cv2.magnitude(img_back[:, :, 0], img_back[:, :, 1])

    return mask[:, :, 0], magnitude_spectrum, img_back

def create_cos_img(u, v, multiplier, N = 32):
    """
    Create a cos image based on given arguments

    Run example: create_cos_img (1, 3, 255, 128)
    """

    Icos = np.zeros((N, N))
    for i in range(0, N):
        for j in range(0, N):
            Icos[i, j] = multiplier * (1 + math.cos( (2 * math.pi) / N * (u*i + v*j)))
    return Icos

def create_cos_img_complicated():
    N = 64
    img_gray = np.zeros((N, N))
    for i in range(0, N):
        for j in range(0, N):
            img_gray[i, j] = 0.5 * math.cos((2 * math.pi / N) * (8 * i + 6 * j)) + \
                                1.5 * math.cos((2 * math.pi / N) * (4 * i + 2 * j)) + \
                                1.0 * math.cos((2 * math.pi / N) * (2 * j))
    return img_gray