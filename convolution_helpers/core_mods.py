"""
This class is responsible for all core operations that involve image manipulation

Created by: Michael Konstantinou, Constandinos Demetriou, George Hadjiantonis
"""
import cv2
import numpy as np
import skimage
from matplotlib import pyplot as plt


def to_grayscale(img):
    """
    Convert image to grayscale
    """

    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return img_gray


def set_plot_image(position, title, img, color_map='gray'):
    """
    Sets an image to a specified position, of a matplotlib window.
    """

    plt.subplot(position)
    plt.title(title)

    # Remove ticks
    plt.xticks([])
    plt.yticks([])

    plt.imshow(img, cmap=color_map)


def plot_all(img_gray, img_noise, img_filtered, noise_type, filter_type):
    """
    Plot all results.
    """

    plt.figure('Noises and Filters for Image')
    set_plot_image(131, 'Input Image', img_gray)
    set_plot_image(132, noise_type + ' Noise', img_noise)
    set_plot_image(133, filter_type + ' Filter', img_filtered)
    plt.show()


def add_noise(img, noise_type):
    """
    Adds noise to the given image.
    """

    img_noise = None

    if noise_type == 'Gaussian':
        img_noise = skimage.util.random_noise(img, mode='gaussian')
    elif noise_type == 'Localvar':
        img_noise = skimage.util.random_noise(img, mode='localvar')
    elif noise_type == 'Poisson':
        img_noise = skimage.util.random_noise(img, mode='poisson')
    elif noise_type == 'Salt & pepper':
        img_noise = skimage.util.random_noise(img, mode='s&p')
    elif noise_type == 'Speckle':
        img_noise = skimage.util.random_noise(img, mode='speckle')

    return img_noise


def filter_img(img, filter_type):
    """
    Filters the given image with the specified type of filter.
    """

    img_filtered = None

    if filter_type == 'Averaging':
        img_filtered = cv2.blur(img, (5, 5), 0)
    elif filter_type == 'Gaussian':
        img_filtered = cv2.GaussianBlur(img, (5, 5), 0)
    elif filter_type == 'Median':
        img_filtered = cv2.medianBlur(np.float32(img), 5)
    elif filter_type == 'Laplacian':
        img_filtered = cv2.Laplacian(img, cv2.CV_64F)
    elif filter_type == 'Sobel':
        img_filtered = cv2.Sobel(img, cv2.CV_64F, 0, 1, ksize=5)

    return img_filtered
